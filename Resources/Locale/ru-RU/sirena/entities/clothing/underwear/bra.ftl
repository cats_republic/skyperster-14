ent-ClothingUnderwearTopBraWhite = бра
    .desc = Только наши лифчики Nanotrasen могут легко сниматься и надеваться.

ent-ClothingUnderwearTopBraSports = спортивное бра
    .desc = Лифчик для занятий спортом. Ведь бег от ухажеров является спортом?

ent-ClothingUnderwearTopBraSportsAlternative = альтернативное спортивное бра
    .desc = Лифчик для занятий спортом. Альтернативная версия. На этот раз будут не ухажеры, а насильники. Пойдет?

#etc
ent-ClothingUnderwearTopBlueShild = бра синего щита
    .desc = Защитный буфер для глав!

ent-ClothingUnderwearTopSpaceNinja = повязка на грудь
    .desc = Кусок обычной ткани.

ent-ClothingUnderwearTopNekoBra = неко бра
    .desc = Для тех кто любит "кошек"

ent-ClothingUnderwearToplaceBra = кружевное бра
    .desc = Для ваших массивных документов

ent-ClothingUnderwearTopDeathBra = бра эскадрона смерти
    .desc = Самые технологичные бра в галактике

# for heads
ent-ClothingUnderwearTopBraCap = бра капитана
    .desc = Вам мало золота на трусах? Мы добавим вам еще, чтобы ваш вес явно увеличился на 1-2 килограмма.

ent-ClothingUnderwearTopBraCE = бра СИ
    .desc = Лифчик для настоящих мужчин.

ent-ClothingUnderwearTopBraCMO = бра ГВ
    .desc = Нужно положить сюда гипоспрей, чтобы агенты его не нашли. Ведь все мы знаем, что делают с некрофилами...

ent-ClothingUnderwearTopBraHOP = бра ГП
    .desc = Если у ваших трусиков нет кармана для карт, то почему бы и не положить карту в лифчик?

ent-ClothingUnderwearTopBraHOS = бра ГСБ
    .desc = Вам жмет, почему бы не обвинить в этом случайного заключенного?

ent-ClothingUnderwearTopBraQM = бра КМ
    .desc = Познайте искусство автономного отдела, но для начала переложите всю ответственность на крепкого мужчину.

ent-ClothingUnderwearTopBraRD = бра НР
    .desc = Аномально разных размеров для каждой научной руководительницы.

# for seniors
ent-ClothingUnderwearTopSeniorScience = бра ведущего учёного
    .desc = Стандартное бра ведущего учёного.

ent-ClothingUnderwearTopSeniorEnginer = бра ведущего инженера
    .desc = Стандартное бра ведущего инженера.

#for jobs
ent-ClothingUnderwearTopSeniorSec = бра служителя закона
    .desc = На самом деле, они даже очень удобные для хранения дроби.

ent-ClothingUnderwearTopSeniorDoctor = бра медицинского работника
    .desc = Кажется, что в них есть кармашек для таблетницы.
