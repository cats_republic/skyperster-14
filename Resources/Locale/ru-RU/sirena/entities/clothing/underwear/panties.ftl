ent-ClothingUnderwearBottomPantiesWhite = трусики
    .desc = Стандартное нижнее бельё Nanotrasen для сотрудниц станции. Они слишком стандартные для слишком стандартной сотрудницы станции, как вы.

#etc
ent-ClothingUnderwearBottomPantiesBlueShild = трусики синего щита
    .desc = Ваш тыл надёжно защищён! Надеюсь...

ent-ClothingUnderwearBottomPantiesSpaceNinja = повязка на пах
    .desc = Кусок обычной ткани. Вроде бы сделано только для девушек.

ent-ClothingUnderwearBottomPantiesNeko = неко трусики
    .desc = Для "кошко" девочек

ent-ClothingUnderwearBottomPantiesThong = стринги
    .desc = Для рисковых девушек

ent-ClothingUnderwearBottomPantiesLace = кружевные трусики
    .desc = Для серьезных леди

ent-ClothingUnderwearBottomPantiesDeathSquad = трусики эскадрона смерти
    .desc = самые технологичные трусики в галактике.

# for heads
ent-ClothingUnderwearBottomPantiesCap = трусики капитана
    .desc = В них еще больше золота, чем в боксерах капитана.

ent-ClothingUnderwearBottomPantiesCE = трусики СИ
    .desc = Эти трусики носят либо фембои, либо женщины, реально хотевшие получить столь сложную для них должность. Вы явно заслужили носить это.

ent-ClothingUnderwearBottomPantiesCMO = трусики ГВ
    .desc = Шесть уколов гипоспрея явно стоят того, чтобы успеть украсть, либо понюхать их.

ent-ClothingUnderwearBottomPantiesHOP = трусики ГП
    .desc = На этот раз без кармана.

ent-ClothingUnderwearBottomPantiesHOS = трусики ГСБ
    .desc = Тут нет ни красного красителя, ни крови особо буйных заключенных. Тогда что-же это?

ent-ClothingUnderwearBottomPantiesQM = трусики КМ
    .desc = О да! Они очень сильно пропитаны потом от столь сильных нагрузок, если вы, конечно, лидер, а не босс.

ent-ClothingUnderwearBottomPantiesRD = трусики НР
    .desc = Может стать артефактом для изучения вашим любимым ученым.

# for seniors

ent-ClothingUnderwearBottomPantiesSeniorScience = трусики ведущего учёного
    .desc = Стандартные трусики ведущего учёного.

ent-ClothingUnderwearBottomPantiesSeniorEnginer = трусики ведущего инженера
    .desc = Стандартные трусики ведущего инженера.

#for jobs

ent-ClothingUnderwearBottomPantiesSeniorDoctor = трусики медицинского работника
    .desc = От них пахнет спиртом.

ent-ClothingUnderwearBottomPantiesSeniorSec = трусики служителя закона
    .desc = Эти трусики явно теснее обычных.
