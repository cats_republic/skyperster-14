ent-PillCanisterAmbuzolPlus = { ent-PillCanister }
    .suffix = Амбулозол плюс, 10
    .desc = { ent-PillCanister.desc }

ent-PillCanisterGabapentin = { ent-PillCanister }
    .suffix = габопептан, 10
    .desc = Нейролептик представляет собой класс фармацевтических препаратов, который характеризуется способностью снижать нейрочувствительность и может применяться в лечении судорог.
ent-PillTricordrazine = таблетка габопептана (15ед)
    .desc = { ent-Pill.desc }

ent-PillCanisterEmoAplife = { ent-PillCanister }
    .suffix = эмоаплайф, 10
    .desc = Это инновационный нейростимулятор, предназначенный для глубокого улучшения эмоционального опыта
ent-PillTricordrazine = таблетка эмоаплайфа (15ед)
    .desc = { ent-Pill.desc }
