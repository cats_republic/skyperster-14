#Cargo


#Engineering
ent-SheetSteelLingering10 = { ent-SheetSteel }
    .desc = { ent-SheetSteel.desc }
ent-SheetGlassLingering10 = { ent-SheetGlass }
    .desc = { ent-SheetGlass.desc }
ent-SheetRGlassLingering10 = { ent-SheetRGlass }
    .desc = { ent-SheetRGlass.desc }
ent-SheetPGlassLingering10 = { ent-SheetPGlass }
    .desc = { ent-SheetPGlass.desc }
ent-SheetRPGlassLingering10 = { ent-SheetRPGlass }
    .desc = { ent-SheetRPGlass.desc }
ent-SheetUGlassLingering10 = { ent-SheetUGlass }
    .desc = { ent-SheetUGlass.desc }
ent-SheetRUGlassLingering10 = { ent-SheetRUGlass }
    .desc = { ent-SheetRUGlass.desc }
ent-SheetPlasteelLingering10 = { ent-SheetPlasteel }
    .desc = { ent-SheetPlasteel.desc }
ent-SheetPlasticLingering10 = { ent-SheetPlastic }
    .desc = { ent-SheetPlastic.desc }
ent-PartRodMetalLingering10 = { ent-PartRodMetal }
    .desc = { ent-PartRodMetal.desc }
ent-SirenaRCD = РСУ киборга
    .desc = Специально модифицированное РСУ для более автономной работы. Генерирует заряды и имеет повышенный запас оных.

#Generic
ent-FireExtinguisherSirena = огнетушитель киборга
    .desc = Имеет встроенный генератор воды.
ent-JetpackSirena = джетпак киборга
    .desc = Использует принцип ионных двигателей для перемещения
ent-HandheldCrewMonitorSirena = { ent-HandheldCrewMonitor }
    .desc = { ent-HandheldCrewMonitor.desc }

#Janitor
ent-SprayBottleSpaceCleanerSirena = { ent-SprayBottleSpaceCleaner }
    .desc = Имеет встроенный генератор космо очистителя.

#Medical
ent-RegenerativeMesh10Lingering = { ent-RegenerativeMesh }
    .desc = { ent-RegenerativeMesh.desc }
ent-MedicatedSuture10Lingering = { ent-MedicatedSuture }
    .desc = { ent-MedicatedSuture.desc }
ent-DexalinPlusMedipenSirena = медипен с дексалином+
    .desc = Имеет встроенный генератор химикатов.
ent-EmergencyMedipenSirena = { ent-EmergencyMedipen }
    .desc = Имеет встроенный генератор химикатов.
ent-HyposprayBicaridineSirena = гипоспрей с бикаридином
    .desc = Данная модель гипоспрея была разработана специально для использования медицинскими киборгами. Имеет встроенный генератор химикатов который настроен на производство бикаридина. 
ent-HyposprayDexalinPlusSirena = гипоспрей с дексалином+
    .desc = Данная модель гипоспрея была разработана специально для использования медицинскими киборгами. Имеет встроенный генератор химикатов который настроен на производство дексалина +. 
ent-HyposprayDermalineSirena = гипоспрей с дермалином
    .desc = Данная модель гипоспрея была разработана специально для использования медицинскими киборгами. Имеет встроенный генератор химикатов который настроен на производство дермалина. 
ent-HyposprayDyloveneSirena = гипоспрей с диловеном
    .desc = Данная модель гипоспрея была разработана специально для использования медицинскими киборгами. Имеет встроенный генератор химикатов который настроен на производство диловена. 
ent-HypospraySigynateSirena = гипоспрей с сигинатом
    .desc = Данная модель гипоспрея была разработана специально для использования медицинскими киборгами. Имеет встроенный генератор химикатов который настроен на производство сигината. 

#Science


#Service
ent-MBRChemistryDispenser = мобильный завод химикатов
    .desc = Мобильная установка по созданию химикатов в промышленных колличествах.
ent-PortableBoozeDispenser = портативный раздатчик алкоголя
    .desc = О, да! Алкоголь для всех!
ent-PortableSodaDispenser = портативный раздатчик безалкоголя
    .desc = Пш-Пшшшшш!

#Security


#Syndicate
ent-EnergyShieldBorg = { ent-EnergyShield }
    .desc = { ent-EnergyShield.desc }

#UIC
