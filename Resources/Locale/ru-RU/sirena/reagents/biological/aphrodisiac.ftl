# ▬▬ Пол: М ▬▬
# PopupMessage
aphrodisiac-popupmessage-s1-m-1 = Вы чувствуете лёгкое возбуждение
aphrodisiac-popupmessage-s1-m-2 = Вы ощущаете, как внизу немного покалывает
aphrodisiac-popupmessage-s1-m-3 = Вы ощущаете как будто нижнее бельё вам жмёт

aphrodisiac-popupmessage-s2-m-1 = Вы чувствуете себя возбуждённым
aphrodisiac-popupmessage-s2-m-2 = Ваше нижнее бельё вам явно жмёт
aphrodisiac-popupmessage-s2-m-3 = Ваше желание нарастает, и вам становится трудно сдерживать себя

aphrodisiac-popupmessage-s3-m-1 = Ваше возбуждение становится сильным, и вас неудержимо манят желания
aphrodisiac-popupmessage-s3-m-2 = Вы ощущаете, как кровь горячей струей бьется в ваших венах
aphrodisiac-popupmessage-s3-m-3 = Ваши желания становятся огненными, и вам хочется бесконтрольно действовать

aphrodisiac-popupmessage-s4-m-1 = Ваше возбуждение становится чрезмерным, и вы чувствуете, что теряете контроль
aphrodisiac-popupmessage-s4-m-2 = Ваше тело замедляется, и желание становится тяжелым бременем
aphrodisiac-popupmessage-s4-m-3 = Вы чувствуете, как ваши движения становятся неуверенными, и окружающий мир кажется искаженным.

# AddPushMarkup
aphrodisiac-pushmarkup-s1-m = Парень выглядит слегка смущенным. [color=#F0E68C]Зрачки глаз слегка расширены.[/color]
aphrodisiac-pushmarkup-s2-m = Парень выглядит возбуждённым. Он томно дышит. [color=#F0E68C]Зрачки глаз слегка расширены.[/color]
aphrodisiac-pushmarkup-s3-m = Парня слегка трясет. Вы отчётливо видите признаки возбуждения. Его взгляд полон вожделения... [color=#DC143C]Зрачки глаз сильно расширены.[/color]
aphrodisiac-pushmarkup-s4-m = Парень демонстрирует явные признаки возбуждения. Во взгляде присутствует ощутимое вожделение и решимость в действиях. Парень дышит намного интенсивнее. [color=#DC143C]Зрачки глаз сильно расширены.[/color]


# ▬▬ Пол: Ж ▬▬
# PopupMessage
aphrodisiac-popupmessage-s1-fm-1 = Вы чувствуете лёгкое возбуждение
aphrodisiac-popupmessage-s1-fm-2 = Вы ощущаете, как внизу немного покалывает
aphrodisiac-popupmessage-s1-fm-3 = Вы ощущаете, как ваши соски натираются об одежду

aphrodisiac-popupmessage-s2-fm-1 = Вы чувствуете себя возбуждённой
aphrodisiac-popupmessage-s2-fm-2 = Вы ощущаете, как ваш низ начинает гореть
aphrodisiac-popupmessage-s2-fm-3 = Вы ощущаете, как желание охватывает вас полностью

aphrodisiac-popupmessage-s3-fm-1 = Ваше возбуждение становится сильным, и вас охватывает страсть
aphrodisiac-popupmessage-s3-fm-2 = Вы чувствуете, как ваше тело готово к несдержимым желаниям
aphrodisiac-popupmessage-s3-fm-3 = Страсть в вас пылает, и вы не можете удержаться от соблазнительных жестов

aphrodisiac-popupmessage-s4-fm-1 = Ваше возбуждение становится чрезмерным, и вы чувствуете, что теряете контроль
aphrodisiac-popupmessage-s4-fm-2 = Ваше тело замедляется, и желание становится тяжелым бременем
aphrodisiac-popupmessage-s4-fm-3 = Ваши движения становятся неустойчивыми, а реальность начинает расплываться.

# AddPushMarkup
aphrodisiac-pushmarkup-s1-fm = Девушка выглядит слегка смущенной. [color=#F0E68C]Зрачки глаз слегка расширены.[/color]
aphrodisiac-pushmarkup-s2-fm = Девушка выглядит возбуждённой. Она томно дышит. [color=#F0E68C]Зрачки глаз слегка расширены.[/color]
aphrodisiac-pushmarkup-s3-fm = Девушку слегка трясет. Вы отчётливо видите признаки возбуждения. Её взгляд полон вожделения... [color=#DC143C]Зрачки глаз сильно расширены.[/color]
aphrodisiac-pushmarkup-s4-fm = Девушка демонстрирует явные признаки возбуждения. Во взгляде присутствует ощутимое вожделение. Она неосознанно соблазнительно виляет бёдрами при походке. Девушка дышит намного интенсивнее. [color=#DC143C]Зрачки глаз сильно расширены.[/color]


# ▬▬ Пол: Unsex ▬▬
# PopupMessage
aphrodisiac-popupmessage-s1-u-1 = TODO
aphrodisiac-popupmessage-s1-u-2 = TODO
aphrodisiac-popupmessage-s1-u-3 = TODO

aphrodisiac-popupmessage-s2-u-1 = TODO
aphrodisiac-popupmessage-s2-u-2 = TODO
aphrodisiac-popupmessage-s2-u-3 = TODO

aphrodisiac-popupmessage-s3-u-1 = TODO
aphrodisiac-popupmessage-s3-u-2 = TODO
aphrodisiac-popupmessage-s3-u-3 = TODO

aphrodisiac-popupmessage-s4-u-1 = TODO
aphrodisiac-popupmessage-s4-u-2 = TODO
aphrodisiac-popupmessage-s4-u-3 = TODO

# AddPushMarkup
aphrodisiac-pushmarkup-s1-u = TODO
aphrodisiac-pushmarkup-s2-u = TODO
aphrodisiac-pushmarkup-s3-u = TODO
aphrodisiac-pushmarkup-s4-u = TODO
