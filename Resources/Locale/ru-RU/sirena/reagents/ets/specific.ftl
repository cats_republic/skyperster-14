reagent-name-bluespacecrystal = блюспейс кристалл
reagent-desc-bluespacecrystal = Перемолотые блюспейс кристаллы
reagent-physical-desc-bluespacecrystal = нестабильный

materials-bluespacecrystal = блюспейс кристалл

reagent-name-flamethrower-fuel = топливо для огнемёта
reagent-desc-flamethrower-fuel = Топливо для огнемёта, разработанное для ОБХАЗ, подобно забавной химической симфонии, изящно танцует в воздухе, окружая цель пламенной объятью и превращая её в недосягаемый островок веселья и испуга.
