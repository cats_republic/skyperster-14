# PopupMessage
emoaplife-popupmessage-s1 = Вы чуть сильнее ощущаете эмоции и тактильный отклик от тела

emoaplife-popupmessage-s2 = Ваше тело ощущает всё более сильно. От боли до наслаждения 

emoaplife-popupmessage-s3 = Любая эмоция или ощущение отзывается в вас сильным импульсом. Вы крайне чувствительны

emoaplife-popupmessage-sideeffect-1 = Вы резко ощущаете сильное разочарование
emoaplife-popupmessage-sideeffect-2 = Вы резко ощущаете сильную радость
emoaplife-popupmessage-sideeffect-3 = Вы резко ощущаете сильную панику
emoaplife-popupmessage-sideeffect-4 = Вы резко ощущаете сильную агрессию по отношение к откружающим
