technologies-bluespace = Передовые компоненты
technologies-bluespacebackpacsholding = Блюспейс хранение вещей
technologies-bluespace-processing = Обработка бс криссталов
technologies-advhud = продвинутый HUD
technologies-cheam = Блюспейс химия
technologies-advhealthanalyzer = продвинутое сканирование
technologies-adv-ore-processing = продвинутая обработка руды

#Borgs
# arsenal
research-technology-cyborgsecsystems = Системы роботизированной службы безопасности

# indastrial
research-technology-basicroboticssystemssirena = Базовые системы робототехники
research-technology-tuhmr = Теория использования гидрофобных материалов в робототехнике
research-technology-advancedtoolscyborgengineers = Продвинутые инструменты киборгов-инженеров

# experimental
esearch-technology-ubsinspacecompression = Использование блюспейс в сжатии пространства для модулей
research-technology-generationresourcesfromenergy = Генерация ресурсов из энергии
research-technology-sophisticatedresourcegenerationalgorithms = Усложёные алгоритмы генерации ресурсов
research-technology-ubsinspacecompression = использование блюспейс для модулей хранения

# biochemical
research-technology-applicationroboticsmedicine = Применение робототехники в медицине
research-technology-advapplicationroboticsmedicine = Продвинутое применение робототехники в медицине
research-technology-chemicalgeneratormodules = Модули генераторы химикатов
research-technology-renimationmodule = Реанимационный модуль

# civilianservices
research-technology-roboticentertainmentsystems = Роботизированные системы развлечения
research-technology-roboticplantingsystems = Роботизированные системы посадок
research-technology-cyborgnavigationsystems = Системы навигации киборгов
research-technology-janitorialmodule = Модуль уборки
research-technology-advjanitorialmodule = Продвинутый Модуль уборки
