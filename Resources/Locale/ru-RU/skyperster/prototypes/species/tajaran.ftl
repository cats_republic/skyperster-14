species-name-tajaran = Таяран


ent-PartTajaran = часть тела таярана
    .desc = { ent-BasePart.desc }
ent-TorsoTajaran = туловище таярана
    .desc = { ent-PartTajaran.desc }
ent-HeadTajaran = голова таярана
    .desc = { ent-PartTajaran.desc }
ent-LeftArmTajaran = левая рука таярана
    .desc = { ent-PartTajaran.desc }
ent-RightArmTajaran = правая рука таярана
    .desc = { ent-PartTajaran.desc }
ent-LeftHandTajaran = левая кисть таярана
    .desc = { ent-PartTajaran.desc }
ent-RightHandTajaran = правая кисть таярана
    .desc = { ent-PartTajaran.desc }
ent-LeftLegTajaran = левая нога таярана
    .desc = { ent-PartTajaran.desc }
ent-RightLegTajaran = правая нога таярана
    .desc = { ent-PartTajaran.desc }
ent-LeftFootTajaran = левая стопа таярана
    .desc = { ent-PartTajaran.desc }
ent-RightFootTajaran = правая стопа таярана
    .desc = { ent-PartTajaran.desc }

ent-BaseMobTajaran = Урист МакМурр
    .desc = { ent-BaseMobSpeciesOrganic.desc }
ent-MobTajaranDummy = Urist McHands
    .desc = A dummy Tajaran meant to be used in character setup.

ent-MobTajaran = Урист МакМурр
    .desc = { ent-BaseMobTajaran.desc }
